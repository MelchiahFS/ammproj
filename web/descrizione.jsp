<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>
    <head>
        <title>NerdBook - Descrizione</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="keywords" content="Nerd, Geek, LOL, Nerdbook, Hacker">
        <meta name="description" content="Descrizione del sito NerdBook">
        <meta name="author" content="Enrico Melis">
        <link rel="stylesheet" type="text/css" href="style.css" media="screen">
    </head>
    <body>
    <jsp:include page="header.jsp"/>
        <div class="pagecontent">
            <h1>Che cos'è NerdBook?</h1>
            <div id='sommario' class='greybox'>
                <h2><em>Sommario</em></h2>
                <ul class="d">
                    <li><a class="link" href='descrizione.jsp#descrizione'>Descrizione</a></li>
                    <li><a class="link" href='descrizione.jsp#funzioni'>Funzionalità</a></li>
                    <li><ul class="d1">
                        <li><a class="link" href='descrizione.jsp#iscrizione'>Iscrizione</a></li>
                        <li><a class="link" href='descrizione.jsp#profilo'>Profilo</a></li>
                        <li><a class="link" href='descrizione.jsp#bacheca'>Bacheca</a></li>
                        <li><a class="link" href='descrizione.jsp#gruppi'>Gruppi</a></li>
                        </ul></li>
                </ul>
            </div>
            <br>
            <br>
            <div id='desc' class='greybox'>
                <a id='descrizione'><h2>Descrizione:</h2></a>
                <hr>
                <p>NerdBook è una piattaforma online dedicata alla condivisione di pensieri, opinioni, storie e immagini
                    con i tuoi colleghi nerd!<br>L'utilizzo della piattaforma è assolutamente gratuito!</p>
                <a id='funzioni'><h2>Funzionalità:</h2></a>
                <hr>
                <a id='iscrizione'><h3><em>Iscrizione</em></h3></a>
                <p>L'iscrizione è semplice e veloce, e soprattutto gratuita! <br>
                    Ti basterà cliccare sulla pagina di login e inserire il tuo username e la tua password.</p>
                <a id='profilo'><h3><em>Profilo</em></h3></a>
                <p>Una volta iscritto, potrai inserire le tue informazioni personali nella pagina del tuo profilo utente.<br>
                    Potrai inoltre inserire una tua immagine e una frase che ti rappresenti!</p>
                <a id='bacheca'><h3><em>Bacheca</em></h3></a>
                <p>Potrai inserire dei messaggi personalizzati nella tua bacheca o in quella dei tuoi amici!</p>
                <a id='gruppi'><h3><em>Gruppi</em></h3></a>
                <p>Potrai creare e far parte di numerosi gruppi di amici, e condividere con loro ciò che vorrai!</p>
            </div>
        </div>
    </body>
</html>

