/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//aspetto che il documento sia pronto
$(document).ready(function() 
{
    //individuo l'elemento del DOM con id input
    $("input").on("keyup click", function() 
    {
        //se un tasto viene premuto o rilasciato estraggo il valore dalla barra di ricerca
        var stringa = $("input").val();

        //strutturo la richiesta ajax
        $.ajax
        ({
            url: "filter.json", // URL in cui la servlet Filter è in ascolto
            data: 
            {
                cmd: "search", //invio commando e dati alla Filter
                text: stringa
            },
            dataType: 'json', // Specifico il tipo di formato utilizzato per la risposta
            //funzione da eseguire nel caso di corretta gestione della richiesta del server
            success: function(data, state) 
            { 
                //chiamo le funzioni di aggiornamento listaUtenti e listaGruppi
                var listaUtenti = data['listaUtenti'];
                aggiornaListaUtenti(listaUtenti); 
                var listaGruppi = data['listaGruppi'];
                aggiornaListaGruppi(listaGruppi);
            },
            error: function(data, state) 
            {

            }        
        });

        // funzione che aggiorna la lista degli utenti trovati
        function aggiornaListaUtenti(listaUtenti) 
        {
            // ripulisco la lista
            $("#listaUtenti").empty();
            // controllo che la lista filtrata contenga almeno un elementoe
            if (listaUtenti.length >= 1) 
            {
                $("#noUsers").hide();
                for (var utente in listaUtenti) // Per ogni oggetto trovato nel database
                {
                    var elem = document.createElement("li");
                    var form = document.createElement("form");
                    var idUtente = document.createElement("input");
                    idUtente.setAttribute("type", "hidden");
                    idUtente.setAttribute("name", "idUtente");
                    idUtente.setAttribute("value", listaUtenti[utente].idUtente);

                    var sub = document.createElement("input");
                    sub.setAttribute("type", "submit");
                    sub.setAttribute("value", listaUtenti[utente].nomeUtente);
                    
                    form.setAttribute("method", "post");
                    form.setAttribute("action", "Bacheca");

                    form.appendChild(idUtente);
                    form.appendChild(sub);

                    elem.append(form);
                    $("#listaUtenti").append(elem);
                }
                
            }
            //attivo un evento click in modo da mostrare all'accesso alla pagina tutti i gruppi
            else
            {
                $("#noUsers").show();
            }
        }
        
        //aggiorna la lista gruppi 
        function aggiornaListaGruppi(listaGruppi) 
        {
            // Svuoto la lista
            $("#listaGruppi").empty();
            // Controllo che la lista filtrata contenga almeno un elemento
            if (listaGruppi.length >= 1) 
            {
                $("#noGroups").hide();
                for (var gruppo in listaGruppi) // Per ogni oggetto trovato nel database
                {

                    var elem = document.createElement("li");

                    var idGruppo = document.createElement("input");
                    idGruppo.setAttribute("type", "hidden");
                    idGruppo.setAttribute("name", "idGruppo");
                    idGruppo.setAttribute("value", listaGruppi[gruppo].idGruppo);

                    var sub = document.createElement("input");
                    sub.setAttribute("type", "submit");
                    sub.setAttribute("value", listaGruppi[gruppo].nomeGruppo);
                    
                    var form = document.createElement("form");
                    form.setAttribute("method", "post");
                    form.setAttribute("action", "Bacheca");

                    form.appendChild(idGruppo);
                    form.appendChild(sub);

                    elem.append(form);
                    $("#listaGruppi").append(elem);
                }
            }
            //se la lista è vuota mostro un messaggio di errore
            else
            {
                $("#noGroups").show();
            }
        }
    });
    
    //attivo un evento click in modo da mostrare all'accesso alla pagina tutti i gruppi
    $("input").trigger("click");
    
});
