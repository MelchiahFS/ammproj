<%@page contentType="application/json" pageEncoding="UTF-8"%>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<json:object>
    <json:array name="listaUtenti" items="${listaUtenti}" var="utente" >
        <json:object>
            <json:property name="idUtente" value="${utente.userID}"/>
            <json:property name="nomeUtente" value="${utente.nome} ${utente.cognome}"/>
        </json:object>
    </json:array>
    <json:array name="listaGruppi" items="${listaGruppi}" var="gruppo">
        <json:object>
            <json:property name="idGruppo" value="${gruppo.idBacheca}"/>
            <json:property name="nomeGruppo" value="${gruppo.nomeGruppo}"/>
        </json:object>
    </json:array>
</json:object>