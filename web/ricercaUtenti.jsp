<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="style.css" media="screen">
        <script type="text/javascript" src="js/jquery-3.2.1.js"></script>
        <script type="text/javascript" src="js/ricercaJavaScript.js"></script>
        <title>NerdBook - Ricerca Utenti</title>
    </head>
    <body>
        <jsp:include page="header.jsp"/>
        <h1>Ricerca Utenti</h1>
        <br>
        <br>
        <div id="searchbar">
            <label for="ricerca"> Cerca utenti o gruppi </label>
            <input type="text" id="ricerca" name="ricerca" autofocus>
        </div>
        <b>Utenti:</b><br>
        <i id="noUsers">Nessun utente trovato</i>
        <ul id="listaUtenti">
            
        </ul>
        <br><br>
        <b>Gruppi</b><br>
        <i id="noGroups">Nessun gruppo trovato</i>
        <ul id="listaGruppi">

        </ul>
    </body>
</html>
