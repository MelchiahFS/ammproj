<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<c:choose>
    <c:when test="${not empty toConfirm}">
        <div>
            <b>Autore:</b> ${nomeScrittore}<br>
            <c:if test="${not empty nomeBacheca}">
                <b>Bacheca di:</b> ${nomeBacheca}<br>
            </c:if>
            <c:if test="${not empty nomeGruppo}">
                <b>Gruppo:</b> ${nomeGruppo}<br>
            </c:if>
            <b>Contenuto:</b><br>${contenuto}<br>
            <c:if test="${not empty tipoAllegato}">
                <b>Allegato:</b><br>
                <c:choose>
                    <c:when test="${tipoAllegato == 'link'}">
                        <a href="${allegato}">${allegato}</a>
                    </c:when>
                    <c:when test="${allegato == 'img'}">
                        <img src="${linkAllegato}" alt="Immagine allegata">
                    </c:when>
                </c:choose>
            </c:if>
            <form action="Bacheca" method="post">
                <input type="text" hidden name="contenuto" value="${contenuto}">
                <c:if test="${not empty tipoAllegato}">
                    <input type="text" hidden name="tipoAllegato" value="${tipoAllegato}">
                    <input type="text" hidden name="allegato" value="${allegato}">
                </c:if>
                <c:choose>
                    <c:when test="${not empty idGruppo}">
                        <input type="text" hidden name="idGruppo" value="${idGruppo}">
                    </c:when>
                    <c:otherwise>
                        <input type="text" hidden name="idUtente" value="${idUtente}">
                    </c:otherwise>
                </c:choose>
                <input type="text" hidden name="idScrittore" value="${loggedUser.userID}">
                <!--
                NON MI SERVE SALVARE ALLA SCRITTURA NOME SCRITTORE E BACHECA SULL'OGGETTO POST
                IN QUANTO VERRANNO CALCOLATI E INSERITI AL CARICAMENTO DAL DB
                -->
                <input type="submit" value="Conferma" name="confirmPost">
            </form>
            <form action="Bacheca" method="post">
                <input type="text" hidden name="idUtente" value="${idUtente}">
                <input type="submit" name="annullaPost" value="Annulla">
            </form>
        </div>
    </c:when>
    <c:otherwise>
        <div>
            <i>Inserisci un nuovo post</i>
        <form action="Bacheca" method="post">
            <label for="contenuto"><b>Scrivi un messaggio:</b></label><br>
            <input type="text" id="contenuto" name="contenuto">
            <br><br>
            <b>Seleziona il tipo di allegato:</b><br>
            <input type="radio" name="tipoAllegato" value="link">Link
            <input type="radio" name="tipoAllegato" value="img">Immagine
            <br>
            <label for="allegato"><b>Inserisci l'URL dell'allegato:</b></label><br>
            <input type="text" id="allegato" name="allegato">
            <c:choose>
                <c:when test="${not empty idGruppo}">
                    <input type="text" hidden name="idGruppo" value="${idGruppo}">
                </c:when>
                <c:otherwise>
                    <input type="text" hidden name="idUtente" value="${idUtente}">
                </c:otherwise>
            </c:choose>
            <br>
            <input type='submit' name='insertPost' value='Inserisci Post'>
        </form>
        </div>
    </c:otherwise>
</c:choose>
<br>
<c:if test="${okPost == 'true'}">
    <c:choose>
        <c:when test="${not empty nomeGruppo}">
            <i>Hai scritto sul gruppo ${nomeGruppo}</i>
        </c:when>
        <c:otherwise>
            <i>Hai scritto sulla bacheca di ${nomeBacheca}</i>
        </c:otherwise>
    </c:choose>
</c:if>
