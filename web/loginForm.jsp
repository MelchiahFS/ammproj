<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>NerdBook - Login</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="keywords" content="Nerd, Geek, LOL, Nerdbook, Hacker">
        <meta name="description" content="Pagina di login di NerdBook">
        <meta name="author" content="Enrico Melis">
        <link rel="stylesheet" type="text/css" href="style.css" media="screen">
    </head>
    <body>
        <jsp:include page="header.jsp"/>
        <div class="pagecontent">
            <h1 class='center'>Effettua il login</h1>
            <div class='greybox' id='loginbox'>
                <c:choose>
                    <c:when test="${loggedIn != true}">
                        <c:if test="${invalidData == true}">
                        <div id="invalidDataWarning">I dati inseriti non sono corretti.</div>
                        </c:if>
                        <c:if test="${loginRequired == true}">
                            <div id="loginRequiredWarning">Accesso vietato. Effettuare prima il login.</div>
                        </c:if>
                        <i>Inserisci le credenziali per effettuare l'accesso al tuo profilo.</i>
                        <hr>
                        <form action ="Login" method="post">
                            <label for='email'><b>Email:</b></label>
                            <br>
                            <input type='text' name='email' id='email' required>
                            <br><br>
                            <label for='psw'><b>Password:</b></label>
                            <br>
                            <input type="password" name='password' id='psw' required>
                            <br><br>
                            <input type='submit' name='submit' value='LOGIN'>
                        </form>
                    </c:when>
                    <c:otherwise>
                        <c:choose>
                            <c:when test="${isAdmin == true}">
                                <i>Sei loggato come:
                                    <b>${loggedUser.nome} ${loggedUser.cognome} (ADMIN)</b>
                                </i>
                            </c:when>
                            <c:otherwise>
                                <i>Sei loggato come:
                                    <b>${loggedUser.nome} ${loggedUser.cognome}</b>
                                </i>
                            </c:otherwise>
                        </c:choose>  
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </body>
</html>
