<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<h3>Le tue amicizie:</h3>
<c:choose>
    <c:when test="${empty listaAmici}">
        <i>Nessuna amicizia presente</i>
    </c:when>
    <c:otherwise>
        <ul>
            <c:forEach items="${listaAmici}" var="amico">
                <div class="listaAmici">
                    <li>
                        <form action="Bacheca" method="post">
                            <input type="text" hidden name="idUtente" value="${amico.userID}">
                            <input type="submit" value="${amico.nome} ${amico.cognome}">
                        </form>
                    </li>
                </div>
            </c:forEach>
        </ul>
    </c:otherwise>
</c:choose>
<h3>Fai parte dei gruppi:</h3>
<c:choose>
    <c:when test="${empty listaGruppi}">
        <i>Nessun gruppo presente</i>
    </c:when>
    <c:otherwise>
        <ul>
            <c:forEach items="${listaGruppi}" var="gruppo">
                <div class="listaAmici">
                    <li>
                        <form action="Bacheca" method="post">
                            <input type="text" hidden name="idGruppo" value="${gruppo.idBacheca}">
                            <input type="submit" value="${gruppo.nomeGruppo}">
                        </form>
                    </li>
                </div>
            </c:forEach>
        </ul>
    </c:otherwise>
</c:choose>
    
