<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div id="banner">
    <div id="divtitle">
        <h1 id='title'>NerdBook</h1>
    </div>
    <nav>
        <ul>
            <li><b><a class="link" href='descrizione.jsp'>Cos'è NerdBook</a></b></li>
            <li><b><a class="link" href='Bacheca'>Bacheca</a></b></li>
            <li><b><a class="link" href='filter.json'>Ricerca Utenti</a></b></li>
            <li><b><a class="link" href='Profilo'>Profilo</a></b></li>
            <c:if test="${loggedIn == false || empty loggedIn}">
                <li><b><a class="link" href='loginForm.jsp'>Login</a></b></li>
            </c:if>
            <c:if test="${loggedIn == true}">
                <li><b><a class="link" href='Logout'>Esci</a></b></li>
            </c:if>
        </ul>
    </nav>
</div>
<div id="emptyspace"></div>
