<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>NerdBook - Bacheca</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="keywords" content="Nerd, Geek, LOL, Nerdbook, Hacker">
        <meta name="description" content="Bacheca di NerdBook">
        <meta name="author" content="Enrico Melis">
        <link rel="stylesheet" type="text/css" href="style.css" media="screen">
    </head>
    <body>
        <jsp:include page="header.jsp"/>
        <div class="pagecontent">
        <c:choose>
            <c:when test="${not empty idGruppo}">
                <h1>Bacheca del gruppo "${nomeGruppo}"</h1>
                <c:choose>
                    <c:when test="${loggedUser.userID !=  creatoreGruppo && isAdmin == false}">
                        <c:if test="${member == true}">
                            <form action="Gruppi" method="post">
                                <input type="text" hidden name="idGruppo" value="${idGruppo}">
                                <input type="submit" name="exitGroup" value="Esci dal gruppo">
                            </form>
                        </c:if>
                        <c:if test="${member == false}">
                            <form action="Gruppi" method="post">
                                <input type="text" hidden name="idGruppo" value="${idGruppo}">
                                <input type="submit" name="enterGroup" value="Iscriviti al gruppo">
                            </form>
                        </c:if>
                    </c:when>
                    <c:otherwise>
                        <c:choose>
                            <c:when test="${empty confirmDeleteGroup}">
                                <form action="Gruppi" method="post">
                                    <input type="text" hidden name="idGruppo" value="${idGruppo}">
                                    <input type="submit" name="requestDelete" value="Elimina gruppo">
                                </form>
                            </c:when>
                            <c:otherwise>
                                <i>Confermi di voler eliminare il gruppo <b>"${nomeGruppo}"</b>?</i>
                                <form action="Gruppi" method="post">
                                    <input type="text" hidden name="idGruppo" value="${idGruppo}">
                                    <input type="submit" name="deleteConfirmed" value="Conferma">
                                </form>
                                <form action="Bacheca" method="post">
                                    <input type="text" hidden name="idGruppo" value="${idGruppo}">
                                    <input type="submit"  value="Annulla">
                                </form>
                            </c:otherwise>
                        </c:choose>
                    </c:otherwise>
                </c:choose>
            </c:when>
            <c:when test="${idUtente == loggedUser.userID}">
                <h1>La mia Bacheca</h1>
            </c:when>
            <c:otherwise>
                <h1>Bacheca di ${nomeBacheca}</h1>
                <c:if test="${isAdmin == false}">
                    <c:if test="${isFriend == true}">
                        <form action="GestioneAmicizie" method="post">
                            <input type="text" hidden name="idAmico" value="${idUtente}">
                            <input type="submit" name="removeFriend" value="Rimuovi Amico">
                        </form>
                    </c:if>
                    <c:if test="${isFriend == false}">
                        <form action="GestioneAmicizie" method="post">
                            <input type="text" hidden name="idAmico" value="${idUtente}">
                            <input type="submit" name="addFriend" value="Aggiungi Amico">
                        </form>
                    </c:if>
                </c:if>
            </c:otherwise>
        </c:choose>
        <br>
        <c:choose>
            <c:when test="${empty idGruppo}">
                <b>Frase personale:</b><br>
                <i>${loggedUser.frasePersonale}</i>
            </c:when>
            <c:otherwise>
                <b>Descrizione del gruppo:</b><br>
                <i>${descrizioneGruppo}
            </c:otherwise>
        </c:choose>
        <jsp:include page="amicizie.jsp"/>
        <br>
        <c:if test="${(isFriend == true) || (idUtente == loggedUser.userID) || (member == true) || (isAdmin == false)}">
            <jsp:include page="nuovoPost.jsp"/>
        </c:if>
        <br>
        <c:choose>
            <c:when test="${not empty listaPost}">
                <c:forEach var="post" items="${listaPost}">
                    <div class="newpost">
                    <b>Autore:</b> ${post.nomeScrittore} <br>
                    <b>Bacheca:</b> ${post.nomeBacheca} <br>
                    <b>Contenuto:</b> ${post.contenuto} <br>
                    <b>Scritto alle ore:</b> ${post.oraCreazione} <br>
                    <c:if test="${not empty post.tipoAllegato}">
                        <b>Allegato:</b><br>
                        <c:if test="${post.tipoAllegato == 'link'}">
                            <a href="${post.linkAllegato}">"${post.linkAllegato}"</a>
                        </c:if>
                        <c:if test="${post.tipoAllegato == 'img'}">
                            <img src="${post.linkAllegato}" alt="Immagine allegata">
                        </c:if>
                            
                    </c:if><br>
                    <c:if test="${(post.idScrittore == loggedUser.userID) || (idUtente == loggedUser.userID) || (isAdmin == true)}">
                        <form action="Bacheca" method="post">
                            <input type="text" hidden name="idPost" value="${post.idPost}">
                            <input type="text" hidden name="idUtente" value="${idUtente}">
                            <input type="submit" name="deletePost" value="Elimina post">
                        </form>
                    </c:if>
                     <br><br>
                    </div>
                </c:forEach>
            </c:when>
            <c:otherwise>
                <b> Nessun post da visualizzare </b>
            </c:otherwise>
        </c:choose>
        <br>
        </div>
    </body>
</html>

