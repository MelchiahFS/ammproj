<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>NerdBook - Profilo</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="keywords" content="Nerd, Geek, LOL, Nerdbook, Hacker">
        <meta name="description" content="Profilo personale NerdBook">
        <meta name="author" content="Enrico Melis">
        <link rel="stylesheet" type="text/css" href="style.css" media="screen">
    </head> 
    <body>
        <jsp:include page="header.jsp"/>
        <div class="pagecontent">
            <h1>Il Tuo Profilo</h1>
            <br>
            <jsp:include page="amicizie.jsp"/>
            <br>
            <div class='greybox'>
                <c:choose>
                    <c:when test="${profileIsSet != null && profileIsSet == false}">
                        <i>Inserisci le tue informazioni personali:</i>
                        <br>
                        <form action="Profilo" method="post">
                            <label for="srnm"><b>Nome:</b></label>
                            <input type="text" id="name" name="name" required>
                            <br>
                            <label for="srnm"><b>Cognome:</b></label>
                            <input type="text" id="surname" name="surname" required>
                            <br>
                            <label for="url"><b>URL immagine di profilo:</b></label>
                            <input type="text" id="url" name="url_image" required>
                            <br>
                            <label for="box"><b>Frase di presentazione:</b></label>
                            <textarea rows="3" cols="20" id="box" name="presentation" required></textarea>
                            <br>
                            <label for="data"><b>Data di nascita:</b></label>
                            <input type="date" id="data" name="birthdate" required>
                            <br>
                            <label for="psw"><b>Password:</b></label>
                            <input type="password" id="psw" name="pass" value="password" required>
                            <br>
                            <label for="cpsw"><b>Conferma password:</b></label>
                            <input type="password" id="cpsw" name="cpass" value="password" required>
                            <br>
                            <input type='submit' name='updateData' value='Aggiorna'>
                        </form>
                        <form action="Profilo" method="post">
                            <input type="submit" value="Annulla">
                        </form>
                    </c:when>
                    <c:when test="${modifyForm == true || not empty passError}">
                        <c:if test="${not empty passError}">
                            <i>Le password non corrispondono</i>
                        </c:if>
                        <i>Aggiorna le tue informazioni personali:</i>
                        <br>
                        <form action="Profilo" method="post">
                            <label for="name"><b>Nome:</b></label>
                            <input type="text" id="name" name="name" value="${loggedUser.nome}"required>
                            <br>
                            <label for="srnm"><b>Cognome:</b></label>
                            <input type="text" id="surname" name="surname" value="${loggedUser.cognome}" required>
                            <br>
                            <label for="data"><b>Data di nascita:</b></label>
                            <input type="date" id="data" name="birthdate" value="${dataFix}" required>
                            <br>
                            <label for="url"><b>URL immagine di profilo:</b></label>
                            <input type="text" id="url" name="url_image" value="${loggedUser.imgProfilo}" required>
                            <br>
                            <label for="box"><b>Frase di presentazione:</b></label>
                            <textarea rows="3" cols="20" id="box" name="presentation" required>
                                ${loggedUser.frasePersonale}
                            </textarea>
                            <br> 
                            <label for="psw"><b>Password:</b></label>
                            <input type="password" id="psw" name="pass" value="${loggedUser.password}" required>
                            <br>
                            <label for="cpsw"><b>Conferma password:</b></label>
                            <input type="password" id="cpsw" name="cpass" value="${loggedUser.password}" required>
                            <br>
                            <input type='submit' name='updateData' value='Aggiorna'>
                        </form>
                        <form action="Profilo" method="post">
                            <input type="submit" value="Annulla">
                        </form>
                    </c:when>
                    <c:otherwise>
                        <c:if test="${updateDone == true}">
                            Informazioni aggiornate correttamente.<br><br>
                        </c:if>
                        <b>Le tue informazioni</b><br><br>
                        <b>Nome: </b>${loggedUser.nome}<br>
                        <b>Cognome: </b>${loggedUser.cognome}<br>
                        <b>Email: </b>${loggedUser.email}<br>
                        <b>Data di nascita: </b>${loggedUser.dataNascita}<br>
                        <b>URL immagine del profilo: </b>${loggedUser.imgProfilo}<br>
                        <b>Frase personale: </b>${loggedUser.frasePersonale}<br>

                        <form action="Profilo" method="post">
                            <input type="submit" name="modify" value="Modifica dati">
                        </form>

                        <br>
                        <b>Crea un nuovo gruppo</b><br>
                        <form action="Gruppi" method="post">
                            <label for="nomeGruppo">Nome del gruppo:</label>
                            <input type="text" id="nomeGruppo" name="nomeGruppo" required>
                            <br>
                            <label for="descrizioneGruppo">Scrivi una descrizione:</label>
                            <textarea rows="3" cols="20" id="descrizioneGruppo" name="descrizioneGruppo" required></textarea>
                            <input type="text" hidden name="idUtente" value="${loggedUser.userID}">
                            <input type="submit" name="creaGruppo" value="Crea Gruppo">
                        </form>
                        <br>
                        <c:choose>
                            <c:when test="${exists == true}">
                                <i>Esiste già un gruppo con questo nome. Inserisci un altro nome.</i><br>
                            </c:when>
                            <c:when test="${okGroup == true}">
                                <i>Gruppo creato con successo.</i>
                            </c:when>
                        </c:choose>
                        <br>
                        <b>Gruppi creati:</b><br>
                        <c:choose>
                            <c:when test="${not empty gruppiCreati}">
                                <ul>
                                    <c:forEach items="${gruppiCreati}" var="gruppo">
                                        <li>
                                            <form action="Bacheca" method="post">
                                                <input type="text" hidden name="idGruppo" value="${gruppo.idBacheca}">
                                                <input type="submit" value="${gruppo.nomeGruppo}">
                                            </form>
                                        </li>
                                    </c:forEach>
                                </ul>
                            </c:when>
                            <c:otherwise>
                                <i>Nessun gruppo creato</i>
                            </c:otherwise>
                        </c:choose>
                        <c:choose>
                            <c:when test="${not empty toConfirmDelete}">
                                <i>Vuoi davvero eliminare il tuo account?</i>
                                <c:if test="${not empty gruppiCreati}">
                                    <i>Se confermerai l'eliminazione verranno eliminati anche i tuoi gruppi:</i>
                                    <ul>
                                        <c:forEach items="${gruppiCreati}" var="gruppo">
                                            <li>${gruppo.nomeGruppo}</li>
                                        </c:forEach>
                                    </ul>
                                </c:if>
                                    <form action="Profilo" method="post">
                                        <input type="submit" name="deleteConfirmed" value="Conferma eliminazione">
                                    </form>
                                    <form action="Profilo" method="post">
                                        <input type="submit" value="Annulla">
                                    </form>
                            </c:when>
                            <c:otherwise>
                                    <form action="Profilo" method="post">
                                    <input type="submit" name="deleteUser" value="Elimina account">
                                </form>
                            </c:otherwise>
                        </c:choose>
                    </c:otherwise>
                </c:choose>                
            </div>
        </div>
    </body>
</html>