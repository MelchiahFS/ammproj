/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nerdbook;

import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import nerdbook.Model.Bacheca;
import nerdbook.Model.BachecaFactory;
import nerdbook.Model.Utente;
import nerdbook.Model.UtenteFactory;

/**
 *
 * @author enrico
 */
@WebServlet(name = "Filter", urlPatterns = {"/filter.json"})
public class Filter extends HttpServlet
{
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        HttpSession session = request.getSession();
        String cmd = request.getParameter("cmd");
        response.setContentType("text/html;charset=UTF-8");
        
        if (session.getAttribute("loggedIn") != null && session.getAttribute("loggedIn").equals(true))
        {
            if (cmd == null)
            {
                request.getRequestDispatcher("ricercaUtenti.jsp").forward(request, response);
                return;
            }
            else if (cmd.equals("search"))
            {
                response.setContentType("application/json;charset=UTF-8");
                String stringa = request.getParameter("text");
                // Esegue la ricerca
                ArrayList<Utente> listaUtenti = UtenteFactory.getInstance().listaUtentiFiltrata(stringa);
                ArrayList<Bacheca> listaGruppi = BachecaFactory.getInstance().listaGruppiFiltrata(stringa);
                // Imposto la lista come attributo della request, come facevamo per l'HTML
                request.setAttribute("listaUtenti", listaUtenti);
                request.setAttribute("listaGruppi", listaGruppi);

                // Quando si restituisce del json e' importante segnalarlo ed evitare il caching
                response.setHeader("Expires", "Sat, 6 May 1995 12:00:00 GMT");
                response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                // Genero il json con una jsp
                request.getRequestDispatcher("listaBachecheJSON.jsp").forward(request, response);
            }
        }
        else
        {
            request.setAttribute("loginRequired", true);
            request.getRequestDispatcher("loginForm.jsp").forward(request, response);
            return;
        }

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

}
