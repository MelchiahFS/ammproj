package nerdbook;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import nerdbook.Model.BachecaFactory;
import nerdbook.Model.Utente;
import nerdbook.Model.UtenteFactory;

@WebServlet(name = "GestioneAmicizie", urlPatterns = {"/GestioneAmicizie"})
public class GestioneAmicizie extends HttpServlet
{

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        BachecaFactory instanceB = BachecaFactory.getInstance();
        UtenteFactory instanceU = UtenteFactory.getInstance();
        int idUtenteLoggato = ((Utente)session.getAttribute("loggedUser")).getUserID();
        int idAmico = Integer.parseInt(request.getParameter("idAmico"));
        int idBachecaAmico = instanceB.getIDBachecaUtenteByCreatore(idAmico);
        if (request.getParameter("addFriend") != null)
        {
            instanceU.addFriend(idUtenteLoggato, idAmico);
            request.setAttribute("idUtente",idAmico);
        }
        if (request.getParameter("removeFriend") != null)
        {
            instanceU.removeFriend(idUtenteLoggato, idAmico);
            request.setAttribute("idUtente",idAmico);
        }
        request.getRequestDispatcher("Bacheca").forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

}
