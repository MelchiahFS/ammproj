/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nerdbook;

import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import nerdbook.Model.BachecaFactory;
import nerdbook.Model.Bacheca;
import nerdbook.Model.Utente;

/**
 *
 * @author enrico
 */
public class Gruppi extends HttpServlet
{

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        BachecaFactory instanceB = BachecaFactory.getInstance();
        Utente u = (Utente)session.getAttribute("loggedUser");
        int idUtente = u.getUserID();
        
        //se l'utente vuole creare un gruppo
        if (request.getParameter("creaGruppo") != null)
        {
            //controllo se esiste già un gruppo con quel nome
            if (instanceB.existsGroup(request.getParameter("nomeGruppo")))
            {
                request.setAttribute("exists", true);
                request.getRequestDispatcher("profilo.jsp").forward(request, response);
                return;
            }
            //se non esiste creo il nuovo gruppo
            else
            {
                Bacheca gruppo = new Bacheca();
                gruppo.setCreatore(idUtente);
                gruppo.setNomeGruppo(request.getParameter("nomeGruppo"));
                gruppo.setDescrizione(request.getParameter("descrizioneGruppo"));
                gruppo.setTipo("gruppo");
                instanceB.newGroup(gruppo);
                
                //aggiorno la lista dei gruppi per la visualizzazione
                ArrayList<Bacheca> listaGruppi= instanceB.listaGruppi(idUtente);
                session.setAttribute("listaGruppi", listaGruppi);
                
                int idGruppo = instanceB.getIDBachecaGruppoByNome(request.getParameter("nomeGruppo"));
                instanceB.addPossessoGruppo(idUtente,idGruppo);
                request.setAttribute("okGroup", true);
                request.getRequestDispatcher("Profilo").forward(request,response);
            }
        }
        //altrimenti
        else
        {
            int idGruppo = Integer.parseInt(request.getParameter("idGruppo"));
            //se l'utente vuole iscriversi a un gruppo
            if (request.getParameter("enterGroup") != null)
            {
                instanceB.addPossessoGruppo(idUtente, idGruppo);
                request.setAttribute("idGruppo",idGruppo);
                request.getRequestDispatcher("Bacheca").forward(request, response);
                return;
            }
            //se vuole disiscriversi da un gruppo
            if (request.getParameter("exitGroup") != null)
            {
                instanceB.removePossessoGruppo(idUtente, idGruppo);
                request.getRequestDispatcher("Bacheca").forward(request, response);
                return;
            }
            //se chiede l'eliminazione di un gruppo chiedo una conferma
            if(request.getParameter("requestDelete") != null)
            {
                request.setAttribute("confirmDeleteGroup",true);
                request.setAttribute("idGruppo",idGruppo);
                request.getRequestDispatcher("Bacheca").forward(request, response);
                return;
            }
            //se l'utente conferma elimino il gruppo e rimando alla bacheca personale dell'utente
            if (request.getParameter("deleteConfirmed") != null)
            {
                instanceB.deleteGroup(idGruppo);
                response.sendRedirect("Bacheca");
                return;
            }
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

}
