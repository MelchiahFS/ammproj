/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nerdbook;

import nerdbook.Model.Post;
import nerdbook.Model.PostFactory;
import nerdbook.Model.BachecaFactory;
import nerdbook.Model.Utente;
import nerdbook.Model.UtenteFactory;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;


public class Bacheca extends HttpServlet
{

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        response.setContentType("text/html;charset=UTF-8");
        
        //Apertura della sessione
        HttpSession session = request.getSession();
        
        if (session.getAttribute("loggedIn") != null && session.getAttribute("loggedIn").equals(true)) 
        {
            UtenteFactory instanceU = UtenteFactory.getInstance();
            BachecaFactory instanceB = BachecaFactory.getInstance();
            PostFactory instanceP = PostFactory.getInstance();
            Utente u = (Utente)session.getAttribute("loggedUser");
            ArrayList<Post> listaPost = new ArrayList<>();
            int idBacheca;
            
            //imposto l'idUtente per caricare i post della sua bacheca in caso di bacheca utente
            int idUtente;
            //caso di redirezione da jsp
            if (request.getParameter("idUtente") != null)
                idUtente = Integer.parseInt(request.getParameter("idUtente"));
            else
            {
                //caso di redirezione da altra servlet
                if(request.getAttribute("idUtente") != null)
                    idUtente = (int)request.getAttribute("idUtente");
                else 
                    //id dell'utente attualmente loggato
                    idUtente = u.getUserID();
            }
            
            
            //se esiste il parametro idGruppo devo caricare la bacheca di un gruppo
            if (request.getParameter("idGruppo") != null)
            {
                //o è passato da una jsp
                if (request.getParameter("idGruppo") != null)
                    idBacheca = Integer.parseInt(request.getParameter("idGruppo"));
                //o da un'altra servlet
                else
                    idBacheca = (int)request.getAttribute("idGruppo");
                nerdbook.Model.Bacheca gruppo = instanceB.getBachecaById(idBacheca);
                request.setAttribute("nomeGruppo", gruppo.getNomeGruppo());
                request.setAttribute("descrizioneGruppo", gruppo.getDescrizione());
                request.setAttribute("idGruppo", idBacheca);
                request.setAttribute("creatoreGruppo", gruppo.getCreatore());
                
                ArrayList<Utente> listaMembri = instanceU.getMembers(idBacheca);
                request.setAttribute("listaMembri", listaMembri);
                
                if (instanceU.isMember(u.getUserID(),idBacheca))
                    request.setAttribute("member", true);
                else
                    request.setAttribute("member", false);
                
            }
            //altrimenti la bacheca appartiene a un utente
            else
            {
                request.setAttribute("idUtente", idUtente);                   

                //ottengo la bacheca dell'utente
                idBacheca = instanceB.getIDBachecaUtenteByCreatore(idUtente);
                String nomeBacheca = instanceB.getBachecaUtenteOwner(idBacheca);
                request.setAttribute("nomeBacheca",nomeBacheca);

                //controllo se la bacheca visitata appartiene a un amico oppure no
                //in tal modo scelgo se mostrare il tasto Richiedi/Elimina amicizia 
                //e se dare la possibilità di inserire nuovi post
                if (u.getUserID() != idUtente)
                {
                    if (instanceB.ownsBacheca(u.getUserID(), idBacheca))
                        request.setAttribute("isFriend", true);
                    else
                        request.setAttribute("isFriend", false);
                }
            }
            //se l'utente sta inserendo un nuovo post
            if (request.getParameter("insertPost") != null)
            {
                //riporto i dati inseriti alla bacheca per richiedere la conferma di inserimento
                request.setAttribute("contenuto",request.getParameter("contenuto"));

                //se esiste un allegato al post aggiungo a toConfirm i suoi dati
                if (request.getParameterMap().containsKey("tipoAllegato"))
                {
                    request.setAttribute("allegato",request.getParameter("allegato"));
                    request.setAttribute("tipoAllegato",request.getParameter("tipoAllegato"));
                }

                //chi scrive il nuovo post è sempre l'utente attuale
                request.setAttribute("nomeScrittore",u.getNome() + " " + u.getCognome());
                if (request.getParameter("idGruppo") != null)
                    request.setAttribute("idGruppo",idBacheca);

                request.setAttribute("toConfirm", true);
            }
            
            //se l'utente conferma l'inserimento del post
            if (request.getParameter("confirmPost") != null)
            {
                //creo un oggetto Post e ci inserisco i dati scritti dall'utente
                Post confirmed = new Post();
                confirmed.setContenuto(request.getParameter("contenuto"));
                if (request.getParameter("tipoAllegato") != null)
                {
                    confirmed.setLinkAllegato(request.getParameter("allegato"));
                    confirmed.setTipoAllegato(request.getParameter("tipoAllegato"));
                }

                //lo scrittore è sempre l'utente attualmente loggato
                confirmed.setIdScrittore(u.getUserID());
                confirmed.setIdBacheca(idBacheca);
                
                //inserisco il post nel database
                instanceP.newPost(confirmed);
                request.setAttribute("okPost", true); //serve per segnalare il corretto inserimento del post in bacheca.jsp
            }
            
            //elimino il post
            if (request.getParameter("deletePost") != null)
            {
                int idPost = Integer.parseInt(request.getParameter("idPost"));
                instanceP.deletePost(idPost);
            }
            
            //caricamento dei post
            if (request.getParameter("idGruppo") != null)
                listaPost = instanceP.getPostsByIdBacheca(idBacheca);
            else
                listaPost = instanceP.getPostsByUser(idUtente);
            request.setAttribute("listaPost", listaPost);
            
            //caricamento lista amici dell'utente loggato
            ArrayList<Utente> listaAmici= instanceU.getListaAmici(u.getUserID());
            session.setAttribute("listaAmici", listaAmici);
            
            ArrayList<nerdbook.Model.Bacheca> listaGruppi= instanceB.listaGruppi(u.getUserID());
            session.setAttribute("listaGruppi", listaGruppi);
            
            request.getRequestDispatcher("bacheca.jsp").forward(request, response);
            return;
        }
        else
        {
            request.setAttribute("loginRequired", true);
            request.getRequestDispatcher("loginForm.jsp").forward(request, response);
            return;
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }
}
