/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nerdbook.Model;

/**
 *
 * @author enrico
 */
public class Bacheca
{
    private int idBacheca;
    private String tipo;
    private String nomeGruppo;
    private String descrizione;
    private int creatore;

    /**
     * @return the idBacheca
     */
    public int getIdBacheca()
    {
        return idBacheca;
    }

    /**
     * @param idBacheca the idBacheca to set
     */
    public void setIdBacheca(int idBacheca)
    {
        this.idBacheca = idBacheca;
    }

    /**
     * @return the padrone
     */
    public int getCreatore()
    {
        return creatore;
    }

    /**
     * @param creatore the creatore to set
     */
    public void setCreatore(int creatore)
    {
        this.creatore = creatore;
    }

    /**
     * @return the isGruppo
     */
    public String getTipo()
    {
        return tipo;
    }

    /**
     * @param tipo the tipo to set
     */
    public void setTipo(String tipo)
    {
        this.tipo = tipo;
    }

    /**
     * @return the nomeGruppo
     */
    public String getNomeGruppo()
    {
        return nomeGruppo;
    }

    /**
     * @param nomeGruppo the nomeGruppo to set
     */
    public void setNomeGruppo(String nomeGruppo)
    {
        this.nomeGruppo = nomeGruppo;
    }

    /**
     * @return the descrizione
     */
    public String getDescrizione()
    {
        return descrizione;
    }

    /**
     * @param descrizione the descrizione to set
     */
    public void setDescrizione(String descrizione)
    {
        this.descrizione = descrizione;
    }
}
