/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nerdbook.Model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author enrico
 */
public class UtenteFactory
{
    private static UtenteFactory singleton;
    
    public static UtenteFactory getInstance() 
    {
        if (singleton == null) 
            singleton = new UtenteFactory();
        return singleton;
    }
    
    private UtenteFactory() {}
    
    //Gestione DB
    private String connectionString;
    
    public void setConnectionString(String s)
    {
	this.connectionString = s;
    }
    
    public String getConnectionString()
    {
            return this.connectionString;
    }
    //Fine gestione DB
    
    //trova un utente tramite il suo id
    public Utente getUtenteById(int id) 
    {
        try
        {
            // path, username, password
            Connection conn = DriverManager.getConnection(connectionString, "ammdb", "ammdb");
            
            String query = "select * from utente where id_utente = ?";
            
            // Prepared Statement
            PreparedStatement stmt = conn.prepareStatement(query);
            
            // Si associano i valori
            stmt.setInt(1, id);
            
            // Esecuzione query
            ResultSet res = stmt.executeQuery();

            // ciclo sulle righe restituite
            if (res.next()) 
            {
                Utente current = new Utente();
                current.setUserID(res.getInt("id_utente"));
                current.setNome(res.getString("nome"));
                current.setCognome(res.getString("cognome"));
                current.setEmail(res.getString("email"));
                current.setPassword(res.getString("password"));
                current.setDataNascita(res.getString("data_nascita"));
                current.setFrasePersonale(res.getString("frase_personale"));
                current.setImgProfilo(res.getString("img_profilo"));
                current.setRuolo(res.getString("ruolo"));

                stmt.close();
                conn.close();
                return current;
            }

            stmt.close();
            conn.close();
        } 
        catch (SQLException e)
        {
            Logger.getLogger(UtenteFactory.class.getName()).log(Level.SEVERE, null, e);
        }    
        return null;
    }
    
    //trova un id tramite email e password (login)
    public int getIdByEmailAndPassword(String email, String password)
    {
        try 
        {
            // path, username, password
            Connection conn = DriverManager.getConnection(connectionString, "ammdb", "ammdb");
            
            String query = "select id_utente from utente where email = ? and password = ?";
            
            // Prepared Statement
            PreparedStatement stmt = conn.prepareStatement(query);
            
            // Si associano i valori
            stmt.setString(1, email);
            stmt.setString(2, password);
            
            // Esecuzione query
            ResultSet res = stmt.executeQuery();

            // ciclo sulle righe restituite
            if (res.next()) 
            {
                int id = res.getInt("id_utente");
                
                stmt.close();
                conn.close();
                return id;
            }

            stmt.close();
            conn.close();
        } 
        catch (SQLException e) 
        {
            Logger.getLogger(UtenteFactory.class.getName()).log(Level.SEVERE, null, e);
        }
        return -1;
    }
    
    //lista di tutti gli utenti di NerdBook
    public ArrayList getUtentiList() 
    {
        ArrayList<Utente> listaUtenti = new ArrayList<>();
        
        try 
        {
            // path, username, password
            Connection conn = DriverManager.getConnection(connectionString, "ammdb", "ammdb");
            
            String query = "select * from utente where nome is not null and cognome is not null";
            
            // Prepared Statement
            PreparedStatement stmt = conn.prepareStatement(query);
            
            // Esecuzione query
            ResultSet res = stmt.executeQuery();

            // ciclo sulle righe restituite
            while (res.next()) 
            {
                Utente current = new Utente();
                current.setUserID(res.getInt("id_utente"));
                current.setNome(res.getString("nome"));
                current.setCognome(res.getString("cognome"));
                current.setEmail(res.getString("email"));
                current.setPassword(res.getString("password"));
                current.setDataNascita(res.getString("data_nascita"));
                current.setFrasePersonale(res.getString("frase_personale"));
                current.setImgProfilo(res.getString("img_profilo"));
                current.setPassword(res.getString("password"));
                current.setRuolo(res.getString("ruolo"));
                
                listaUtenti.add(current);
            }

            stmt.close();
            conn.close();
        } 
        catch (SQLException e) 
        {
            Logger.getLogger(UtenteFactory.class.getName()).log(Level.SEVERE, null, e);
        }
        
        return listaUtenti;
    }
    
    //lista di utenti risultante dalla ricerca sulla searchbar 
    public ArrayList listaUtentiFiltrata(String ricerca) 
    {
        ArrayList<Utente> listaUtenti = new ArrayList<>();
        
        try 
        {
            Connection conn = DriverManager.getConnection(connectionString, "ammdb", "ammdb");
            
            String query = "select * from utente where nome is not null and cognome is not null "
                    + "and (UPPER(nome) like UPPER(?)) or (UPPER(cognome) like UPPER(?))";
            
            PreparedStatement stmt = conn.prepareStatement(query);
            ricerca = "%"+ricerca+"%";
            stmt.setString(1, ricerca);
            stmt.setString(2, ricerca);
            ResultSet res = stmt.executeQuery();

            while (res.next()) 
            {
                Utente current = new Utente();
                current.setUserID(res.getInt("id_utente"));
                current.setNome(res.getString("nome"));
                current.setCognome(res.getString("cognome"));
                current.setEmail(res.getString("email"));
                current.setPassword(res.getString("password"));
                current.setDataNascita(res.getString("data_nascita"));
                current.setFrasePersonale(res.getString("frase_personale"));
                current.setImgProfilo(res.getString("img_profilo"));
                current.setPassword(res.getString("password"));
                current.setRuolo(res.getString("ruolo"));
                
                listaUtenti.add(current);
            }

            stmt.close();
            conn.close();
        } 
        catch (SQLException e) 
        {
            Logger.getLogger(UtenteFactory.class.getName()).log(Level.SEVERE, null, e);
        }
        
        return listaUtenti;
    }
    
    //lista dei membri di un gruppo specifico
    public ArrayList getUtentiListByGroup(int idGruppo) 
    {
        ArrayList<Utente> listaUtentiG = new ArrayList<>();
        
        try 
        {
            Connection conn = DriverManager.getConnection(connectionString, "ammdb", "ammdb");
            
            String query = "select * from utente join membro on id_utente = utente "
                    + "join gruppo on id_gruppo = gruppo where id_gruppo = ?";
            
            PreparedStatement stmt = conn.prepareStatement(query);
            
            stmt.setInt(1, idGruppo);
            
            ResultSet res = stmt.executeQuery();

            while (res.next()) 
            {
                Utente current = new Utente();
                current.setUserID(res.getInt("user_id"));
                current.setNome(res.getString("nome"));
                current.setCognome(res.getString("cognome"));
                current.setEmail(res.getString("email"));
                current.setPassword(res.getString("password"));
                current.setDataNascita(res.getString("dataNascita"));
                current.setFrasePersonale(res.getString("frasePersonale"));
                current.setImgProfilo(res.getString("imgProfilo"));
                current.setPassword(res.getString("password"));
                current.setRuolo(res.getString("ruolo"));
                
                listaUtentiG.add(current);
            }

            stmt.close();
            conn.close();
        } 
        catch (SQLException e) 
        {
            Logger.getLogger(UtenteFactory.class.getName()).log(Level.SEVERE, null, e);
        }
        
        return listaUtentiG;
    }
    
    //lista di amici di un utente specifico
    public ArrayList getListaAmici(int idUtente)
    {
        ArrayList<Utente> listaAmici = new ArrayList<>();
        
        try 
        {
            Connection conn = DriverManager.getConnection(connectionString, "ammdb", "ammdb");
            
            String query = "select U2.* from utente U1 join amicizia on U1.id_utente = utente1 " +
                                    "join utente U2 on utente2 = U2.id_utente where U1.id_utente = ?";
            
            PreparedStatement stmt = conn.prepareStatement(query);
            
            stmt.setInt(1, idUtente);

            ResultSet res = stmt.executeQuery();

            while (res.next()) 
            {
                Utente current = new Utente();
                current.setUserID(res.getInt("id_utente"));
                current.setNome(res.getString("nome"));
                current.setCognome(res.getString("cognome"));
                current.setEmail(res.getString("email"));
                current.setPassword(res.getString("password"));
                current.setDataNascita(res.getString("data_nascita"));
                current.setFrasePersonale(res.getString("frase_personale"));
                current.setImgProfilo(res.getString("img_profilo"));
                current.setPassword(res.getString("password"));
                current.setRuolo(res.getString("ruolo"));
                
                listaAmici.add(current);
            }

            stmt.close();
            conn.close();
        } 
        catch (SQLException e) 
        {
            Logger.getLogger(UtenteFactory.class.getName()).log(Level.SEVERE, null, e);
        }
        
        return listaAmici;
    }
    
    //lista dei membri di un gruppo
    public ArrayList<Utente> getMembers(int idBacheca)
    {
        ArrayList<Utente> membri = new ArrayList<>();
        try
        {
            Connection conn = DriverManager.getConnection(connectionString, "ammdb", "ammdb");
            String query = "select utente.* from utente join possesso on id_utente = utente join "
                    + "bacheca on id_bacheca = bacheca where id_bacheca = ? and tipo = 'gruppo'";
            PreparedStatement stmt = conn.prepareStatement(query);
            stmt.setInt(1, idBacheca);
            ResultSet res = stmt.executeQuery();
            while (res.next())
            {
                Utente u = new Utente();
                u.setUserID(res.getInt("id_utente"));
                u.setNome(res.getString("nome"));
                u.setCognome(res.getString("cognome"));
                u.setDataNascita(res.getString("data_nascita"));
                u.setFrasePersonale(res.getString("frase_personale"));
                u.setImgProfilo(res.getString("img_profilo"));
                u.setEmail(res.getString("email"));
                u.setRuolo(res.getString("ruolo"));
            }
            stmt.close();
            conn.close();
        }
        catch (SQLException e) 
        {
            Logger.getLogger(UtenteFactory.class.getName()).log(Level.SEVERE, null, e);
        }
        return membri;
    }
    
    //aggiorna i dati dell'utente
    public void updateProfile(Utente u)
    {
        try
        {
            Connection conn = DriverManager.getConnection(connectionString, "ammdb", "ammdb");
            String query = "update utente set nome = ?, cognome = ?, email =?, password = ?, "
                    + "data_nascita = ?, frase_personale = ?, img_profilo = ? "
                    + "where id_utente = ?";
            PreparedStatement stmt = conn.prepareStatement(query);
            stmt.setString(1, u.getNome());
            stmt.setString(2, u.getCognome());
            stmt.setString(3, u.getEmail());
            stmt.setString(4, u.getPassword());
            stmt.setString(5, u.getDataNascita());
            stmt.setString(6, u.getFrasePersonale());
            stmt.setString(7, u.getImgProfilo());
            stmt.setInt(8, u.getUserID());
            stmt.executeUpdate();
            stmt.close();
            conn.close();
        }
        catch(SQLException e)
        {
            Logger.getLogger(UtenteFactory.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    //controlla se un utente è amico di un altro utente
    public boolean isFriend(int idUtenteLoggato, int idAltroUtente)
    {
        try
        {
            Connection conn = DriverManager.getConnection(connectionString, "ammdb", "ammdb");
            String query = "select utente2 from utente join amicizia on id_utente = utente1 where id_utente=? "
                    + "and utente2 = ?";
            PreparedStatement stmt = conn.prepareStatement(query);
            stmt.setInt(1, idUtenteLoggato);
            stmt.setInt(2, idAltroUtente);
            ResultSet res = stmt.executeQuery();
            if (res.next())
            {
                stmt.close();
                conn.close();
                return true;
            }
            stmt.close();
            conn.close();
        }
        catch(SQLException e)
        {
            Logger.getLogger(UtenteFactory.class.getName()).log(Level.SEVERE, null, e);
        }
        return false;
    }
    
    //controlla se un utente è membro del gruppo oppure no
    public boolean isMember(int idUtenteLoggato, int idGruppo)
    {
        try
        {
            Connection conn = DriverManager.getConnection(connectionString, "ammdb", "ammdb");
            String query = "select * from possesso where utente = ? and bacheca = ?";
            PreparedStatement stmt = conn.prepareStatement(query);
            stmt.setInt(1, idUtenteLoggato);
            stmt.setInt(2, idGruppo);
            ResultSet res = stmt.executeQuery();
            if (res.next())
            {
                stmt.close();
                conn.close();
                return true;
            }
            stmt.close();
            conn.close();
        }
        catch(SQLException e)
        {
            Logger.getLogger(UtenteFactory.class.getName()).log(Level.SEVERE, null, e);
        }
        return false;
    }
    
    //dà a un utente il diritto di poter pubblicare sulla bacheca di un amico
    public void addPossesso(int idUtenteLoggato, int idAmico)
    {
        BachecaFactory instanceB = BachecaFactory.getInstance();
        int idBachecaAmico = instanceB.getIDBachecaUtenteByCreatore(idAmico);
        try
        {
            Connection conn = DriverManager.getConnection(connectionString, "ammdb", "ammdb");
            String query = "insert into possesso values(?,?)";
            PreparedStatement stmt = conn.prepareStatement(query);
            stmt.setInt(1, idUtenteLoggato);
            stmt.setInt(2, idBachecaAmico);
            stmt.executeUpdate();
            stmt.close();
            conn.close();
        }
        catch(SQLException e)
        {
            Logger.getLogger(UtenteFactory.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    //aggiunge un amico (la richiesta è unidirezionale da A a B)
    public void addFriend(int idUtenteLoggato, int idAmico)
    {
        try
        {
            Connection conn = DriverManager.getConnection(connectionString, "ammdb", "ammdb");
            String query = "insert into amicizia values(?,?)";
            PreparedStatement stmt = conn.prepareStatement(query);
            stmt.setInt(1, idUtenteLoggato);
            stmt.setInt(2, idAmico);
            stmt.executeUpdate();
            stmt.close();
            conn.close();
        }
        catch(SQLException e)
        {
            Logger.getLogger(UtenteFactory.class.getName()).log(Level.SEVERE, null, e);
        }
        addPossesso(idUtenteLoggato,idAmico);
    }
    
    
    //rimuove il diritto di pubblicare sulla bacheca di un amico (in seguito alla rimozione di un amico)
    public void removePossesso(int idUtenteLoggato, int idAmico)
    {
        BachecaFactory instanceB = BachecaFactory.getInstance();
        int idBachecaAmico = instanceB.getIDBachecaUtenteByCreatore(idAmico);
        try
        {
            Connection conn = DriverManager.getConnection(connectionString, "ammdb", "ammdb");
            String query = "delete from possesso where utente = ? and bacheca = ?";
            PreparedStatement stmt = conn.prepareStatement(query);
            stmt.setInt(1, idUtenteLoggato);
            stmt.setInt(2, idBachecaAmico);
            stmt.executeUpdate();
            stmt.close();
            conn.close();
        }
        catch(SQLException e)
        {
            Logger.getLogger(UtenteFactory.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    //rimuove un amico
    public void removeFriend(int idUtenteLoggato, int idAmico)
    {
        try
        {
            Connection conn = DriverManager.getConnection(connectionString, "ammdb", "ammdb");
            String query = "delete from amicizia where utente1 = ? and utente2 = ?";
            PreparedStatement stmt = conn.prepareStatement(query);
            stmt.setInt(1, idUtenteLoggato);
            stmt.setInt(2, idAmico);
            stmt.executeUpdate();
            stmt.close();
            conn.close();
        }
        catch(SQLException e)
        {
            Logger.getLogger(UtenteFactory.class.getName()).log(Level.SEVERE, null, e);
        }
        removePossesso(idUtenteLoggato,idAmico);
    }
    
    //cancella un utente da NerdBook
    public void deleteUser(int idUtente, int idBacheca)
    {
        Connection conn = null;
        PreparedStatement stmt1;
        PreparedStatement stmt2;
        PreparedStatement stmt3;
        PreparedStatement stmt4;
        PreparedStatement stmt5;
        

        //elimino tutti i post scritti nella bacheca dell'utente e tutti i post scritti dall'utente    
        String  query1 = "delete from post where bacheca in "
                + "(select id_bacheca from bacheca join utente on creatore = id_utente where id_utente = ?)"
                + " or scrittore = ?";
        //elimino tutte le amicizie
        String query2 = "delete from amicizia where utente1 = ? or utente2 = ?";       
        //elimino il possesso della bacheca da parte dell'utente e dei suoi amici
        String query3 = "delete from possesso where utente = ? or bacheca = ?";      
        //elimino la bacheca dell'utente
        String query4 = "delete from bacheca where id_bacheca = ?";
        //elimino infine l'utente
        String query5 = "delete from utente where id_utente = ?";
        
        try
        {
            conn = DriverManager.getConnection(connectionString, "ammdb", "ammdb");
            conn.setAutoCommit(false);
            
            stmt1 = conn.prepareStatement(query1);
            stmt2 = conn.prepareStatement(query2);
            stmt3 = conn.prepareStatement(query3);
            stmt4 = conn.prepareStatement(query4);
            stmt5 = conn.prepareStatement(query5);
            
            stmt1.setInt(1, idUtente);
            stmt1.setInt(2, idUtente);
            stmt1.executeUpdate();
            
            stmt2.setInt(1, idUtente);
            stmt2.setInt(2, idUtente);
            stmt2.executeUpdate();
            
            stmt3.setInt(1, idUtente);
            stmt3.setInt(2, idBacheca);
            stmt3.executeUpdate();
            
            stmt4.setInt(1, idBacheca);
            stmt4.executeUpdate();
            
            stmt5.setInt(1, idUtente);
            stmt5.executeUpdate();
            
            conn.commit();
            
        }
        catch(SQLException e)
        {
            Logger.getLogger(UtenteFactory.class.getName()).log(Level.SEVERE, null, e);
            if (conn != null)
            {
                try 
                {
                    System.err.print("Transaction is being rolled back");
                    conn.rollback();
                    conn.setAutoCommit(true);
                    conn.close();
                } 
                catch(SQLException excep) 
                {
                    Logger.getLogger(UtenteFactory.class.getName()).log(Level.SEVERE,null,excep);
                }
            }
        }
    }
    
    
    public String convertDate(String dataNascita, String formato1, String formato2)
    {
        try
        {
            SimpleDateFormat actualFormat = new SimpleDateFormat(formato1);
            SimpleDateFormat fixFormat = new SimpleDateFormat(formato2);
            Date data = (Date)actualFormat.parse(dataNascita); //creo un oggetto Date con formato actualFormat
            String dataFix = fixFormat.format(data); //ottengo una stringa col fixFormat dall'oggetto Date
            dataNascita = dataFix;
        } 
        catch (ParseException ex)
        {
            Logger.getLogger(Utente.class.getName()).log(Level.SEVERE, null, ex);
        }
        return dataNascita;
    }
}
