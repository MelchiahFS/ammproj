/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nerdbook.Model;
import java.lang.reflect.Field;

/**
 *
 * @author enrico
 */
public class Utente
{
    private int userID;
    private String nome;
    private String cognome;
    private String email;
    private String password;
    private String dataNascita;
    private String frasePersonale;
    private String imgProfilo;
    private String ruolo;
    
    /**
     * @return the nome
     */
    public String getNome()
    {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome)
    {
        this.nome = nome;
    }

    /**
     * @return the cognome
     */
    public String getCognome()
    {
        return cognome;
    }

    /**
     * @param cognome the cognome to set
     */
    public void setCognome(String cognome)
    {
        this.cognome = cognome;
    }

    /**
     * @return the email
     */
    public String getEmail()
    {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email)
    {
        this.email = email;
    }

    /**
     * @return the password
     */
    public String getPassword()
    {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password)
    {
        this.password = password;
    }

    /**
     * @return the dataNascita
     */
    public String getDataNascita()
    {
        return dataNascita;
    }

    /**
     * @param dataNascita the dataNascita to set
     */
    public void setDataNascita(String dataNascita)
    {
        if (dataNascita != null)
        {
            this.dataNascita = UtenteFactory.getInstance().convertDate(dataNascita, "yyyy-MM-dd", "dd/MM/yyyy");
        }
        else
            this.dataNascita = null;
    }

    /**
     * @return the frasePersonale
     */
    public String getFrasePersonale()
    {
        return frasePersonale;
    }

    /**
     * @param frasePersonale the frasePersonale to set
     */
    public void setFrasePersonale(String frasePersonale)
    {
        this.frasePersonale = frasePersonale;
    }

    /**
     * @return the imgProfilo
     */
    public String getImgProfilo()
    {
        return imgProfilo;
    }

    /**
     * @param imgProfilo the imgProfilo to set
     */
    public void setImgProfilo(String imgProfilo)
    {
        this.imgProfilo = imgProfilo;
    }

    /**
     * @return the userID
     */
    public int getUserID()
    {
        return userID;
    }

    /**
     * @param userID the userID to set
     */
    public void setUserID(int userID)
    {
        this.userID = userID;
    }
    
    public boolean profileIsSet() throws IllegalAccessException
    {
        for (Field f: this.getClass().getDeclaredFields())
        {
            f.setAccessible(true);
            if (f.get(this) == null)
                return false;
        }
        return true;
    }

    /**
     * @return the ruolo
     */
    public String getRuolo()
    {
        return ruolo;
    }

    /**
     * @param ruolo the ruolo to set
     */
    public void setRuolo(String ruolo)
    {
        this.ruolo = ruolo;
    }
}
