/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nerdbook.Model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author enrico
 */
public class PostFactory
{
    private static PostFactory singleton;
    
    public static PostFactory getInstance()
    {
        if (singleton == null)
            singleton = new PostFactory();
        return singleton;
    }
    
    private PostFactory(){}
    
    private String connectionString;
    
    public void setConnectionString(String s)
    {
        this.connectionString = s;
    }
    
    public String getConnectionString()
    {
        return connectionString;
    }

    //trova nome e cognome di chi ha scritto un post
    public String getScrittoreById(int idScrittore)
    {
        try
        {
            Connection conn = DriverManager.getConnection(connectionString,"ammdb","ammdb");
            String query = "select nome, cognome from utente join post on id_utente = scrittore"
                    + " where scrittore = ?";
            PreparedStatement stmt = conn.prepareStatement(query);
            stmt.setInt(1,idScrittore);
            ResultSet res = stmt.executeQuery();
            
            while(res.next())
            {
                String nome = res.getString("nome");
                String cognome = res.getString("cognome");
                return nome + " " + cognome;
            }
            
            stmt.close();
            conn.close();
            
        }
        catch(SQLException e)
        {
            Logger.getLogger(PostFactory.class.getName()).log(Level.SEVERE, null, e);
        }
        return null;
    }
    
    //trova il nome del padrone di una bacheca utente
    public String getNomeBachecaById(int idBacheca)
    {
        BachecaFactory instanceB = BachecaFactory.getInstance();
        String tipo = instanceB.getBachecaType(idBacheca);
        String nomeBacheca = null;
        
        if (tipo.equals("utente"))
        {
            try
            {
                Connection conn = DriverManager.getConnection(connectionString,"ammdb","ammdb");
                String query = "select nome, cognome from utente join bacheca on id_utente = creatore "
                        + "join post on id_bacheca = bacheca where tipo = 'utente' and bacheca = ?";
                PreparedStatement stmt = conn.prepareStatement(query);
                stmt.setInt(1,idBacheca);
                ResultSet res = stmt.executeQuery();

                if (res.next())
                {
                    String nome = res.getString("nome");
                    String cognome = res.getString("cognome");
                    nomeBacheca = nome + " " + cognome;
                }

                stmt.close();
                conn.close();

            }
            catch(SQLException e)
            {
                Logger.getLogger(PostFactory.class.getName()).log(Level.SEVERE, null, e);
            }
            
        }
        if (tipo.equals("gruppo"))
        {
            try
            {
                Connection conn = DriverManager.getConnection(connectionString,"ammdb","ammdb");
                String query = "select nome_gruppo from bacheca where id_bacheca = ?";
                PreparedStatement stmt = conn.prepareStatement(query);
                stmt.setInt(1,idBacheca);
                ResultSet res = stmt.executeQuery();

                if (res.next())
                {
                    nomeBacheca = res.getString("nome_gruppo");
                }

                stmt.close();
                conn.close();

            }
            catch(SQLException e)
            {
                Logger.getLogger(PostFactory.class.getName()).log(Level.SEVERE, null, e);
            }
        }
            
        return nomeBacheca;
    }
    
    //restituisce una lista di post che un utente può visualizzare nella sua bacheca 
    //(suoi o di amici o di membri di gruppi di cui fa parte)
    public ArrayList getPostsByUser(int idUtente)
    {
        UtenteFactory instance = UtenteFactory.getInstance();
        ArrayList<Post> listaPost = new ArrayList<>();
        try
        {
            Connection conn = DriverManager.getConnection(connectionString,"ammdb","ammdb");
            String query = "select distinct post.* from post " +
            "join bacheca on post.bacheca = id_bacheca " +
            "join possesso on possesso.bacheca = post.bacheca where " +
            "(tipo = 'gruppo' and id_bacheca in " +
            "(select bacheca from possesso where utente = ?)) " +
            "or creatore = ? order by ora_creazione desc";
            PreparedStatement stmt = conn.prepareStatement(query);
            stmt.setInt(1,idUtente);
            stmt.setInt(2,idUtente);
            ResultSet res = stmt.executeQuery();
            
            while (res.next())
            {
                Post p = new Post();
                p.setIdPost(res.getInt("id_post"));
                p.setContenuto(res.getString("contenuto"));
                p.setLinkAllegato(res.getString("url_allegato"));
                p.setTipoAllegato(res.getString("tipo_allegato"));
                p.setIdScrittore(res.getInt("scrittore"));
                p.setIdBacheca(res.getInt("bacheca"));
                p.setNomeScrittore(getScrittoreById(res.getInt("scrittore")));
                p.setNomeBacheca(getNomeBachecaById(res.getInt("bacheca")));
                p.setOraCreazione(res.getTimestamp("ora_creazione"));
                listaPost.add(p);
            }
            
            stmt.close();
            conn.close();
            
        }
        catch(SQLException e)
        {
            Logger.getLogger(PostFactory.class.getName()).log(Level.SEVERE, null, e);
        }
        
        return listaPost;
    }
    
    //restituisce i post relativi a una bacheca specifica
    public ArrayList getPostsByIdBacheca(int idBacheca)
    {
        UtenteFactory instance = UtenteFactory.getInstance();
        ArrayList<Post> listaPostBacheca = new ArrayList<>();
        try
        {
            Connection conn = DriverManager.getConnection(connectionString,"ammdb","ammdb");
            String query = "select distinct post.* from post join bacheca on id_bacheca = bacheca"
                    + " where id_bacheca = ? order by ora_creazione desc";
            PreparedStatement stmt = conn.prepareStatement(query);
            stmt.setInt(1,idBacheca);
            ResultSet res = stmt.executeQuery();
            
            while (res.next())
            {
                Post p = new Post();
                p.setIdPost(res.getInt("id_post"));
                p.setContenuto(res.getString("contenuto"));
                p.setLinkAllegato(res.getString("url_allegato"));
                p.setTipoAllegato(res.getString("tipo_allegato"));
                p.setIdScrittore(res.getInt("scrittore"));
                p.setIdBacheca(res.getInt("bacheca"));
                p.setNomeScrittore(getScrittoreById(res.getInt("scrittore")));
                p.setNomeBacheca(getNomeBachecaById(res.getInt("bacheca")));
                p.setOraCreazione(res.getTimestamp("ora_creazione"));
                listaPostBacheca.add(p);
            }
            
            stmt.close();
            conn.close();
            
        }
        catch(SQLException e)
        {
            Logger.getLogger(PostFactory.class.getName()).log(Level.SEVERE, null, e);
        }
        
        return listaPostBacheca;
    }
    
    //crea un nuovo post nel db
    public void newPost(Post p)
    {
        try
        {
            Connection conn = DriverManager.getConnection(connectionString, "ammdb", "ammdb");
            String query = "insert into post (id_post, contenuto, url_allegato, tipo_allegato, scrittore, bacheca) "
                    + "values (default,?, ?, ?, ?, ?)";
            PreparedStatement stmt = conn.prepareStatement(query);
            stmt.setString(1, p.getContenuto());
            stmt.setString(2, p.getLinkAllegato());
            stmt.setString(3, p.getTipoAllegato());
            stmt.setInt(4, p.getIdScrittore());
            stmt.setInt(5, p.getIdBacheca());
            stmt.executeUpdate();
            stmt.close();
            conn.close();
        }
        catch(SQLException e)
        {
            Logger.getLogger(PostFactory.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    //elimina un post
    public void deletePost(int idPost)
    {
        try
        {
            Connection conn = DriverManager.getConnection(connectionString, "ammdb", "ammdb");
            String query = "delete from post where id_post = ?";
            PreparedStatement stmt = conn.prepareStatement(query);
            stmt.setInt(1, idPost);
            stmt.executeUpdate();
            stmt.close();
            conn.close();
        }
        catch(SQLException e)
        {
            Logger.getLogger(PostFactory.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
}
