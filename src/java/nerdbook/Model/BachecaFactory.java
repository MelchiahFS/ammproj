/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nerdbook.Model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author enrico
 */
public class BachecaFactory
{
    private static BachecaFactory singleton;
    
    public static BachecaFactory getInstance()
    {
        if (singleton == null)
            singleton = new BachecaFactory();
        return singleton;
    }
    
    private BachecaFactory(){}
    
    private String connectionString;
    
    /**
     * @return the connectionString
     */
    public String getConnectionString()
    {
        return connectionString;
    }

    /**
     * @param connectionString the connectionString to set
     */
    public void setConnectionString(String connectionString)
    {
        this.connectionString = connectionString;
    }
    
    //trova una bacheca tramite il suo id
    public Bacheca getBachecaById(int idBacheca)
    {
        Bacheca b = new Bacheca();
        try
        {
            Connection conn = DriverManager.getConnection(getConnectionString(), "ammdb", "ammdb");
            String query = "select * from bacheca where id_bacheca = ?";
            PreparedStatement stmt = conn.prepareStatement(query);
            stmt.setInt(1,idBacheca);
            ResultSet res = stmt.executeQuery();
            if (res.next())
            {
                b.setIdBacheca(idBacheca);
                b.setCreatore(res.getInt("creatore"));
                b.setTipo(res.getString("tipo"));
                b.setNomeGruppo(res.getString("nome_gruppo"));
                b.setDescrizione(res.getString("descrizione"));
            }
            stmt.close();
            conn.close();
        }
        catch (SQLException e)
        {
            Logger.getLogger(BachecaFactory.class.getName()).log(Level.SEVERE, null, e);
        }
        return b;
    }
    
    //trova la bacheca personale di uno specifico utente
    public int getIDBachecaUtenteByCreatore(int creatore)
    {
        try
        {
            UtenteFactory instance = UtenteFactory.getInstance();
            Connection conn = DriverManager.getConnection(getConnectionString(), "ammdb", "ammdb");
            String query = "select id_bacheca from bacheca where creatore = ? and tipo = 'utente' ";
            PreparedStatement stmt = conn.prepareStatement(query);
            stmt.setInt(1,creatore);
            ResultSet res = stmt.executeQuery();
            if (res.next())
            {
                int idBacheca = res.getInt("id_bacheca");
                stmt.close();
                conn.close();
                return idBacheca;
            }
            stmt.close();
            conn.close();
        }
        catch (SQLException e)
        {
            Logger.getLogger(BachecaFactory.class.getName()).log(Level.SEVERE, null, e);
        }
        return -1;
    }
     
    //trova l'id di un gruppo tramite il suo nome
    public int getIDBachecaGruppoByNome(String nomeGruppo)
    {
        try
        {
            Connection conn = DriverManager.getConnection(connectionString, "ammdb", "ammdb");
            String query = "select id_bacheca from bacheca where nome_gruppo = ? and tipo = 'gruppo'";
            PreparedStatement stmt = conn.prepareStatement(query);
            stmt.setString(1, nomeGruppo);
            ResultSet res = stmt.executeQuery();
            if (res.next())
            {
                int idBacheca = res.getInt("id_bacheca");
                stmt.close();
                conn.close();
                return idBacheca;
            }
            stmt.close();
            conn.close();
        }
        catch (SQLException e)
        {
            Logger.getLogger(BachecaFactory.class.getName()).log(Level.SEVERE, null, e);
        }
        return -1;
    }
    
    //trova chi è il padrone della bacheca utente tramite l'id  
    public String getBachecaUtenteOwner(int idBacheca)
    {
        try
        {
            Connection conn = DriverManager.getConnection(connectionString, "ammdb", "ammdb");
            String query = "select nome,cognome from utente join bacheca on id_utente = creatore "
                    + "where tipo = 'utente' and id_bacheca = ?";
            PreparedStatement stmt = conn.prepareStatement(query);
            stmt.setInt(1, idBacheca);
            ResultSet res = stmt.executeQuery();
            if (res.next())
            {
                String nome = res.getString("nome");
                String cognome = res.getString("cognome");
                stmt.close();
                conn.close();
                return nome + " " + cognome;
            }
            stmt.close();
            conn.close();
        } 
        catch (SQLException ex)
        {
            Logger.getLogger(BachecaFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    //controlla se un utente può pubblicare su una bacheca oppure no
    public boolean ownsBacheca(int idUtente, int idBacheca)
    {
        try
        {
            Connection conn = DriverManager.getConnection(connectionString, "ammdb", "ammdb");
            String query = "select * from possesso where utente = ? and bacheca = ?";
            PreparedStatement stmt = conn.prepareStatement(query);
            stmt.setInt(1, idUtente);
            stmt.setInt(2, idBacheca);
            ResultSet res = stmt.executeQuery();
            if (res.next())
            {
                stmt.close();
                conn.close();
                return true;
            }
            stmt.close();
            conn.close();
        }
        catch(SQLException e)
        {
            Logger.getLogger(BachecaFactory.class.getName()).log(Level.SEVERE, null, e);
        }
        return false;
    }
    
    //controlla che il gruppo che l'utente vuole creare non esista già (stesso nome)
    public boolean existsGroup(String nomeGruppo)
    {
        try
        {
            Connection conn = DriverManager.getConnection(connectionString, "ammdb", "ammdb");
            String query = "select * from bacheca where nome_gruppo = ?";
            PreparedStatement stmt = conn.prepareStatement(query);
            stmt.setString(1, nomeGruppo);
            ResultSet res = stmt.executeQuery();
            if (res.next())
            {
                stmt.close();
                conn.close();
                return true;
            }
            stmt.close();
            conn.close();
        }
        catch (SQLException e)
        {
            Logger.getLogger(BachecaFactory.class.getName()).log(Level.SEVERE,null,e);
        }
        return false;
    }
    
    //crea un nuovo gruppo
    public void newGroup(Bacheca gruppo)
    {
        try
        {
            Connection conn = DriverManager.getConnection(connectionString, "ammdb", "ammdb");
            String query = "insert into bacheca (id_bacheca,creatore,tipo,nome_gruppo,descrizione)"
                    + " values(default,?,?,?,?)";
            PreparedStatement stmt = conn.prepareStatement(query);
            
            stmt.setInt(1, gruppo.getCreatore());
            stmt.setString(2, gruppo.getTipo());
            stmt.setString(3, gruppo.getNomeGruppo());
            stmt.setString(4, gruppo.getDescrizione());
            stmt.executeUpdate();
            
            stmt.close();
            conn.close();
        }
        catch (SQLException e)
        {
            Logger.getLogger(BachecaFactory.class.getName()).log(Level.SEVERE,null,e);
        }
    }
    
    //aggiunge un membro al gruppo
    public void addPossessoGruppo(int idUtenteLoggato, int idBachecaGruppo)
    {
        try
        {
            Connection conn = DriverManager.getConnection(connectionString, "ammdb", "ammdb");
            String query = "insert into possesso values(?,?)";
            PreparedStatement stmt = conn.prepareStatement(query);
            stmt.setInt(1, idUtenteLoggato);
            stmt.setInt(2, idBachecaGruppo);
            stmt.executeUpdate();
            stmt.close();
            conn.close();
        }
        catch(SQLException e)
        {
            Logger.getLogger(UtenteFactory.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    //rimuove un membro dal gruppo
    public void removePossessoGruppo(int idUtenteLoggato, int idBachecaGruppo)
    {
        try
        {
            Connection conn = DriverManager.getConnection(connectionString, "ammdb", "ammdb");
            String query = "delete from possesso where utente = ? and bacheca = ?";
            PreparedStatement stmt = conn.prepareStatement(query);
            stmt.setInt(1, idUtenteLoggato);
            stmt.setInt(2, idBachecaGruppo);
            stmt.executeUpdate();
            stmt.close();
            conn.close();
        }
        catch(SQLException e)
        {
            Logger.getLogger(UtenteFactory.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    //lista dei gruppi creati dall'utente
    public ArrayList<Bacheca> listaGruppiUtente(int idUtente)
    {
        ArrayList<Bacheca> gruppi = new ArrayList<>();
        try
        {
            Connection conn = DriverManager.getConnection(connectionString, "ammdb", "ammdb");
            String query = "select bacheca.* from bacheca where tipo = 'gruppo' and creatore = ?";
            PreparedStatement stmt = conn.prepareStatement(query);
            stmt.setInt(1, idUtente);
            ResultSet res = stmt.executeQuery();
            while (res.next())
            {
                Bacheca b = new Bacheca();
                b.setIdBacheca(res.getInt("id_bacheca"));
                b.setCreatore(idUtente);
                b.setTipo("gruppo");
                b.setNomeGruppo(res.getString("nome_gruppo"));
                b.setDescrizione(res.getString("descrizione"));
                gruppi.add(b);
            }
            stmt.close();
            conn.close();
            
        }
        catch (SQLException e) 
        {
            Logger.getLogger(BachecaFactory.class.getName()).log(Level.SEVERE,null,e);
        }
        return gruppi;
    }
    
    //lista di tutti i gruppi di NerdBooke
    public ArrayList<Bacheca> listaGruppi()
    {
        ArrayList<Bacheca> listaGruppi = new ArrayList<>();
        try
        {
            Connection conn = DriverManager.getConnection(connectionString, "ammdb", "ammdb");
            String query = "select * from bacheca where tipo = 'gruppo'";
            PreparedStatement stmt = conn.prepareStatement(query);
            ResultSet res = stmt.executeQuery();
            while (res.next())
            {
                Bacheca b = new Bacheca();
                b.setIdBacheca(res.getInt("id_bacheca"));
                b.setCreatore(res.getInt("creatore"));
                b.setTipo(res.getString("tipo"));
                b.setNomeGruppo(res.getString("nome_gruppo"));
                b.setDescrizione(res.getString("descrizione"));
                listaGruppi.add(b);
            }
            stmt.close();
            conn.close();
        }
        catch (SQLException e) 
        {
            Logger.getLogger(BachecaFactory.class.getName()).log(Level.SEVERE,null,e);
        }
        return listaGruppi;
    }
    
    //lista di gruppi di cui l'utente è membro
    public ArrayList<Bacheca> listaGruppi(int idUtente)
    {
        ArrayList<Bacheca> listaGruppi = new ArrayList<>();
        try
        {
            Connection conn = DriverManager.getConnection(connectionString, "ammdb", "ammdb");
            String query = "select bacheca.* from bacheca join possesso on id_bacheca = bacheca "
                    + "where tipo = 'gruppo' and utente = ?";
            PreparedStatement stmt = conn.prepareStatement(query);
            stmt.setInt(1, idUtente);
            ResultSet res = stmt.executeQuery();
            while (res.next())
            {
                Bacheca b = new Bacheca();
                b.setIdBacheca(res.getInt("id_bacheca"));
                b.setCreatore(res.getInt("creatore"));
                b.setTipo(res.getString("tipo"));
                b.setNomeGruppo(res.getString("nome_gruppo"));
                b.setDescrizione(res.getString("descrizione"));
                listaGruppi.add(b);
            }
            stmt.close();
            conn.close();
        }
        catch (SQLException e) 
        {
            Logger.getLogger(BachecaFactory.class.getName()).log(Level.SEVERE,null,e);
        }
        return listaGruppi;
    }
        
    //lista gruppi risultante dalla ricerca sulla searchbar
    public ArrayList<Bacheca> listaGruppiFiltrata(String txt)
    {
        ArrayList<Bacheca> listaGruppi = new ArrayList<>();
        try
        {
            Connection conn = DriverManager.getConnection(connectionString, "ammdb", "ammdb");
            String query = "select * from bacheca where tipo = 'gruppo' and (UPPER(nome_gruppo) like UPPER(?))";
            PreparedStatement stmt = conn.prepareStatement(query);
            
            txt = "%"+txt+"%";
            
            stmt.setString(1, txt);
            ResultSet res = stmt.executeQuery();
            while (res.next())
            {
                Bacheca b = new Bacheca();
                b.setIdBacheca(res.getInt("id_bacheca"));
                b.setCreatore(res.getInt("creatore"));
                b.setTipo(res.getString("tipo"));
                b.setNomeGruppo(res.getString("nome_gruppo"));
                b.setDescrizione(res.getString("descrizione"));
                listaGruppi.add(b);
            }
            stmt.close();
            conn.close();
        }
        catch (SQLException e) 
        {
            Logger.getLogger(BachecaFactory.class.getName()).log(Level.SEVERE,null,e);
        }
        return listaGruppi;
    }
    
    //controlla se la bacheca è di un utente o di un gruppo
    public String getBachecaType(int idBacheca)
    {
        String tipo = null;
        try
        {
            Connection conn = DriverManager.getConnection(connectionString, "ammdb", "ammdb");
            String query = "select tipo from bacheca where id_bacheca = ?";
            PreparedStatement stmt = conn.prepareStatement(query);
            stmt.setInt(1, idBacheca);
            ResultSet res = stmt.executeQuery();
            
            if (res.next())
                tipo = res.getString("tipo");
            
            stmt.close();
            conn.close();
        }
        catch (SQLException e) 
        {
            Logger.getLogger(BachecaFactory.class.getName()).log(Level.SEVERE,null,e);
        }
        return tipo;
    }
    
    //cancella un gruppo
    public void deleteGroup(int idGruppo)
    {
        Connection conn = null;
        //prima elimino i post nel gruppo
        String query1 = "delete from post where bacheca = ?";
        //elimino poi la partecipazione al gruppo degli utenti
        String query2 = "delete from possesso where bacheca = ?";
        //infine elimino la bacheca del gruppo
        String query3 = "delete from bacheca where id_bacheca = ?";
        
        PreparedStatement stmt1;
        PreparedStatement stmt2;
        PreparedStatement stmt3;
        
        try
        {
            conn = DriverManager.getConnection(connectionString, "ammdb", "ammdb");
            conn.setAutoCommit(false);
            
            stmt1 = conn.prepareStatement(query1);
            stmt2 = conn.prepareStatement(query2);
            stmt3 = conn.prepareStatement(query3);
            
            stmt1.setInt(1, idGruppo);
            stmt1.executeUpdate();
            stmt2.setInt(1, idGruppo);
            stmt2.executeUpdate();
            stmt3.setInt(1, idGruppo);
            stmt3.executeUpdate();
            
            stmt1.close();
            stmt2.close();
            stmt3.close();
            
            conn.commit();
        }
        catch (SQLException e) 
        {
            Logger.getLogger(BachecaFactory.class.getName()).log(Level.SEVERE,null,e);
            if (conn != null)
            {
                try 
                {
                    System.err.print("Transaction is being rolled back");
                    conn.rollback();
                    conn.setAutoCommit(true);
                    conn.close();
                } 
                catch(SQLException excep) 
                {
                    Logger.getLogger(BachecaFactory.class.getName()).log(Level.SEVERE,null,e);
                }
            }
        }
    }
    
}
