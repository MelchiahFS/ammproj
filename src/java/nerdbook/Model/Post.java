/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nerdbook.Model;

import java.sql.Timestamp;



public class Post
{
    
    private int idPost;
    private String contenuto;
    private String linkAllegato;
    private String tipoAllegato;
    private int idScrittore;
    private int idBacheca;
    private String nomeScrittore;
    private String nomeBacheca;
    private Timestamp oraCreazione;
    

    /**
     * @return the contenuto
     */
    public String getContenuto()
    {
        return contenuto;
    }

    /**
     * @param contenuto the contenuto to set
     */
    public void setContenuto(String contenuto)
    {
        this.contenuto = contenuto;
    }

    /**
     * @return the linkAllegato
     */
    public String getLinkAllegato()
    {
        return linkAllegato;
    }

    /**
     * @param linkAllegato the linkAllegato to set
     */
    public void setLinkAllegato(String linkAllegato)
    {
        this.linkAllegato = linkAllegato;
    }

    /**
     * @return the idPost
     */
    public int getIdPost()
    {
        return idPost;
    }

    /**
     * @param idPost the idPost to set
     */
    public void setIdPost(int idPost)
    {
        this.idPost = idPost;
    }

    /**
     * @return the tipoAllegato
     */
    public String getTipoAllegato()
    {
        return tipoAllegato;
    }

    /**
     * @param tipoAllegato the tipoAllegato to set
     */
    public void setTipoAllegato(String tipoAllegato)
    {
        this.tipoAllegato = tipoAllegato;
    }

    /**
     * @return the idScrittore
     */
    public int getIdScrittore()
    {
        return idScrittore;
    }

    /**
     * @param idScrittore the scrittore to set
     */
    public void setIdScrittore(int idScrittore)
    {
        this.idScrittore = idScrittore;
    }

    /**
     * @return the oraCreazione
     */
    public Timestamp getOraCreazione()
    {
        return oraCreazione;
    }

    /**
     * @param oraCreazione the oraaCreazione to set
     */
    public void setOraCreazione(Timestamp oraCreazione)
    {
        this.oraCreazione = oraCreazione;
    }

    /**
     * @return the idBacheca
     */
    public int getIdBacheca()
    {
        return idBacheca;
    }

    /**
     * @param idBacheca the idBacheca to set
     */
    public void setIdBacheca(int idBacheca)
    {
        this.idBacheca = idBacheca;
    }

    /**
     * @return the nomeScrittore
     */
    public String getNomeScrittore()
    {
        return nomeScrittore;
    }

    /**
     * @param nomeScrittore the nomeScrittore to set
     */
    public void setNomeScrittore(String nomeScrittore)
    {
        this.nomeScrittore = nomeScrittore;
    }

    /**
     * @return the nomeBacheca
     */
    public String getNomeBacheca()
    {
        return nomeBacheca;
    }

    /**
     * @param nomeBacheca the nomeBacheca to set
     */
    public void setNomeBacheca(String nomeBacheca)
    {
        this.nomeBacheca = nomeBacheca;
    }
}
