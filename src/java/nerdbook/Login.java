package nerdbook;

import nerdbook.Model.PostFactory;
import nerdbook.Model.Utente;
import nerdbook.Model.UtenteFactory;
import nerdbook.Model.BachecaFactory;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "Login", urlPatterns = {"/Login"}, loadOnStartup = 0)
public class Login extends HttpServlet
{
    private static final String JDBC_DRIVER = "org.apache.derby.jdbc.EmbeddedDriver";
    private static final String DB_CLEAN_PATH = "../../web/WEB-INF/db/AMMDB";
    private static final String DB_BUILD_PATH = "WEB-INF/db/AMMDB";
    
    @Override
    public void init()
    {
        //String dbConnection = "jdbc:derby:" + this.getServletContext().getRealPath("/") + DB_BUILD_PATH;
        
        //utilizzo questa stringa di connessione perché è l'unica che ha funzionato sul mio NetBeans, 
        //con le altre stringhe da lei fornite ho avuto mille problemi che non mi hanno permesso di lavorare
        String dbConnection = "jdbc:derby://localhost:1527/AMMDB";
        try {
            Class.forName(JDBC_DRIVER);
        } 
        catch (ClassNotFoundException ex) 
        {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        }
        UtenteFactory.getInstance().setConnectionString(dbConnection);
        BachecaFactory.getInstance().setConnectionString(dbConnection);
        PostFactory.getInstance().setConnectionString(dbConnection);
    }
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        response.setContentType("text/html;charset=UTF-8");

        //Apertura della sessione
        HttpSession session = request.getSession();

        //Se esiste un attributo di sessione loggedIn e questo vale true
        //(Utente già loggato)
        if (session.getAttribute("loggedIn") == null) 
        {
            String email = request.getParameter("email");
            String password = request.getParameter("password");

            if (email != null && password != null)
            {
                int loggedUserID = UtenteFactory.getInstance().getIdByEmailAndPassword(email, password);

                //se l'utente è valido...
                if (loggedUserID != -1)
                {
                    Utente loggedUser = UtenteFactory.getInstance().getUtenteById(loggedUserID);
                    
                    if (loggedUser.getRuolo().equals("admin"))
                        session.setAttribute("isAdmin", true);
                    else
                        session.setAttribute("isAdmin", false);
                    
                    session.setAttribute("loggedIn", true);
                    session.setAttribute("loggedUser", loggedUser);
                    //Se il profilo è già stato impostato dall'utente
                    try
                    {
                        if (loggedUser.profileIsSet())
                        {
                            session.setAttribute("profileIsSet", true);
                            request.getRequestDispatcher("Bacheca").forward(request, response);
                        }
                        //Altrimenti reindirizzo alla pagina del profilo
                        else
                        {
                            session.setAttribute("profileIsSet", false);
                            request.getRequestDispatcher("profilo.jsp").forward(request, response);
                        }
                    }
                    catch (IllegalAccessException e)
                    {
                        Logger.getLogger(Utente.class.getName()).log(Level.SEVERE, null, e);
                    }
                    return;
                } 
                else 
                {
                    //ritorno al form del login informandolo che i dati non sono validi
                    request.setAttribute("invalidData", true);
                    request.getRequestDispatcher("loginForm.jsp").forward(request, response);
                    return;
                }
            }
        }

        /*
          Se non si verifica nessuno degli altri casi,
          tentativo di accesso diretto alla servlet Login -> reindirizzo verso
          il form di login.
        */
        request.getRequestDispatcher("loginForm.jsp").forward(request, response);
    }
    
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    
    
}