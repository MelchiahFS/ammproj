/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nerdbook;

import nerdbook.Model.Utente;
import nerdbook.Model.UtenteFactory;
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import nerdbook.Model.BachecaFactory;
import nerdbook.Model.Bacheca;


@WebServlet(name = "Profilo", urlPatterns = {"/Profilo"})
public class Profilo extends HttpServlet
{

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        response.setContentType("text/html;charset=UTF-8");
        //Apertura della sessione
        HttpSession session = request.getSession();
        
        //Se esiste un attributo di sessione loggedIn e questo vale true
        //(Utente già loggato)
        if (session.getAttribute("loggedIn") != null && session.getAttribute("loggedIn").equals(true)) 
        {
            Utente u = (Utente)session.getAttribute("loggedUser");
            BachecaFactory instanceB = BachecaFactory.getInstance();
            UtenteFactory instanceU = UtenteFactory.getInstance();
            
            if (request.getParameter("updateData") != null || !session.getAttribute("profileIsSet").equals(true))
            {
                String psw1 = request.getParameter("pass");
                String psw2 = request.getParameter("cpass");
                if (psw1.equals(psw2))
                {
                    u.setNome(request.getParameter("name"));
                    u.setCognome(request.getParameter("surname"));
                    u.setDataNascita(request.getParameter("birthdate"));
                    u.setFrasePersonale(request.getParameter("presentation"));
                    u.setImgProfilo(request.getParameter("url_image"));
                    u.setPassword(request.getParameter("pass"));
                    instanceU.updateProfile(u);
                    session.setAttribute("loggedUser", u);
                    request.setAttribute("updateDone", true);
                    session.setAttribute("profileIsSet", true);
                }
                else
                {
                    request.setAttribute("passError", true);
                    request.setAttribute("dataFix", instanceU.convertDate(u.getDataNascita(), "dd/MM/yyyy", "yyyy-MM-dd"));
                }
            }
            
            if (request.getParameter("modify") != null)
            {
                request.setAttribute("modifyForm", true);
                request.setAttribute("dataFix", instanceU.convertDate(u.getDataNascita(), "dd/MM/yyyy", "yyyy-MM-dd"));
                request.getRequestDispatcher("profilo.jsp").forward(request, response);
                return;
            }
            
            int idUtente = u.getUserID();
            
            //stampa la lista dei gruppi creati dall'utente
            ArrayList<Bacheca> gruppiCreati = instanceB.listaGruppiUtente(idUtente);
            
            //se l'utente vuole cancellarsi da NerdBook chiedo la conferma
            if (request.getParameterMap().containsKey("deleteUser"))
            {
                request.setAttribute("toConfirmDelete", true);
            }
            
            //se l'utente conferma la cancellazione, elimino prima i gruppi da lui creati e poi l'utente stesso
            if (request.getParameterMap().containsKey("deleteConfirmed"))
            {
                for (Bacheca g : gruppiCreati)
                {
                    instanceB.deleteGroup(g.getIdBacheca());
                }
                instanceU.deleteUser(idUtente,instanceB.getIDBachecaUtenteByCreatore(idUtente));
                //dopo la cancellazione rimando alla schermata di login
                request.getRequestDispatcher("Logout").forward(request, response);
                return;
            }
            request.setAttribute("gruppiCreati", gruppiCreati);
            request.getRequestDispatcher("profilo.jsp").forward(request,response);
            return;
        }
        //se l'utente non è loggato non gli è permesso visitare la pagina
        else
        {
            request.setAttribute("loginRequired", true);
            request.getRequestDispatcher("loginForm.jsp").forward(request, response);
            return;
        }
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

}
